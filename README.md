# How-to

This branch contains automation scripts (Vagrant + Ansible) which deploy Wordpress@Apache on local host's VirtualBox. So it is necessary to have VirtualBox running on your local host machine where you run Vagrant.

There is `provisioning/.vault_pass.txt` file, which must contain the right secret in order to decrypt password variables using Ansible vault.
These static passwords are used during provisioning process. The encrypted variable file is the following:

```shell
provisioning/roles/sfromm.mariadb/vars/secrets.yml
```

## Spin Up a Box

The automation was tested using VirtualBox 5.1.22, Ansible 2.3.0.0 and Vagrant 1.9.5. Ansible and Vagrant versions are checked during deployment. If minimum versions are not met, the automation scripts will be interrupted.

Start deployment using standard Vagrant command:

```sh
$ vagrant up
```

This will bring up an Azure VM as per the configuration options set above.

You can now SSH using `vagrant ssh`

For Wordpress site, navigate the browser to the localhost URL http://localhost:8081 or you can ssh into Vagrant box and figure out internal vagrant box IP address. Then navigate the browser to the URL:
http://interal_box_ipaddress

Once finished, run `vagrant destroy` in order to clean up VirtualBox resources.